import {GET_COMMODITIES} from "../constant/constant";

export const getCommodities = () => (dispatch) => {
  // TODO: code smell for url definition
  fetch('http://localhost:8080/api/commodities')
    .then(response => response.json())
    .then((data) => {
      dispatch({
        type: GET_COMMODITIES,
        payload: data
      })
    })
};
export const createCommodity = (data, callback) => (dispatch) => {
  fetch(`http://localhost:8080/api/commodities`, {
    method: 'post',
    headers: {'Content-Type': 'application/json;charset=UTF-8'},
    body: JSON.stringify(data)
  })
    .then((resolve) => {
      console.log(resolve)
      if (resolve.status === 500) {
        alert('商品名称已存在，请输入新的商品名称')
      }else if(resolve.status === 201){
        dispatch(getCommodities());
        callback();
      }
    })
};