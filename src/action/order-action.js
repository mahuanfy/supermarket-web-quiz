import {GET_ORDERS} from "../constant/constant";
// TODO: no used import
import {getCommodities} from "./commodity-action";

export const getOrders = () => (dispatch) => {
  fetch('http://localhost:8080/api/orders')
    .then(response => response.json())
    .then((data) => {
      dispatch({
        type: GET_ORDERS,
        payload: data
      })
    })
};
export const deleteOrderByCommodityId = (id) => (dispatch) => {
  fetch(`http://localhost:8080/api/orders/commodities/${id}`, {method: 'delete'})
    .then((data) => {
      dispatch(
        getOrders()
      )
    }).catch(()=>{
      alert("订单删除失败，请稍后再试")
  })
};
export const createOrder = (id) => (dispatch) => {
  fetch(`http://localhost:8080/api/orders/commodities/${id}`,{
    method:'post',
    headers: {'Content-Type': 'application/json;charset=UTF-8'}
  })
    .then(() => {
      // TODO: why alert?
      alert("添加成功")
    })
};