import React, { Component } from 'react';
import {BrowserRouter as Router} from 'react-router-dom'
import './App.less';
import Header from "./component/header/Header";
import {Route, Switch} from "react-router";
import ComponentList from './component/commdoity-list/commodity-list';
import CommodityCreate from './component/commdoity-list/create/commodity-create';
import Orders from './component/order/order';
import Footer from "./component/footer/footer";

class App extends Component {
  render() {
    return (
      <Router>
        <Header/>
        <Switch>
          <Route exact path='/' component={ComponentList}/>
          <Route path='/orders' component={Orders}/>
          <Route path='/create' component={CommodityCreate}/>
        </Switch>
        <Footer/>
      </Router>
    );
  }
}

export default App;
