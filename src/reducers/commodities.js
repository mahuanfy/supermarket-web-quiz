import {GET_COMMODITIES} from "../constant/constant";

const initState = [];
export default (state = initState, action) => {
  // TODO: why switch ?
  switch (action.type) {
    case GET_COMMODITIES:
      return action.payload;
    default:
      return state;
  }
}