import {combineReducers} from "redux";
import commodities from './commodities';
import orders from './orders';

const reducers = combineReducers({
  commodities,
  orders
});
export default reducers