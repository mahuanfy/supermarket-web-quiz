import {GET_ORDERS} from "../constant/constant";

const initState = [];
export default (state = initState, action) => {
  switch (action.type) {
    case GET_ORDERS:
      return action.payload;
    default:
      return state;
  }
}