import React from 'react'
import './footer.less';

const Footer = () => {
  return (
    <section className='footer'>
      <span>TW Mall @2018 Created by ForCheng</span>
    </section>
  )
}

export default Footer