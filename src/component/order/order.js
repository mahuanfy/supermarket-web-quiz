import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteOrderByCommodityId, getOrders} from "../../action/order-action";
import './orders.less';

class Order extends Component {
  componentDidMount() {
    this.props.getOrders();
  }

  handleOnClick(commodityId) {
    // TODO: no console.log
    console.log(commodityId)
    this.props.deleteOrderByCommodityId(commodityId)
  }

  render() {
    const {orders} = this.props;
    // TODO: code smell for too many logic in render method
    const commodityGroup = {};
    const commodities = orders.map(order => {
      return order.commodity
    });
    commodities.forEach(function (obj) {
      // TODO: not readable
      const array = commodityGroup[obj['id']] || [];
      array.push(obj);
      commodityGroup[obj['id']] = array;
    });
    const result = Object.values(commodityGroup)
    return (
      <section className='order'>
        <table className="table">
          <tbody>
          <tr>
            <th>名字</th>
            <th>单价</th>
            <th>数量</th>
            <th>单位</th>
            <th>操作</th>
          </tr>
          {result.map((order, index) => {
            const {id, name, price, unit} = order[0];
            return <tr key={index}>
              <td>{name}</td>
              <td>{price}</td>
              <td>{order.length}</td>
              <td>{unit}</td>
              <td>
                <button onClick={() => this.handleOnClick(id)}>删除</button>
              </td>
            </tr>
          })}
          </tbody>
        </table>

      </section>
    );
  }
}

const mapStateToProps = (state) => ({
  orders: state.orders
});
const mapDispatchToProps = (dispatch) => ({
  getOrders: () => dispatch(getOrders()),
  deleteOrderByCommodityId: (id) => dispatch(deleteOrderByCommodityId(id)),

});
export default connect(mapStateToProps, mapDispatchToProps)(Order);