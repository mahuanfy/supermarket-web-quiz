import React from 'react'
import {Link, NavLink} from "react-router-dom";
import './header.less';
import {MdAdd, MdAddShoppingCart, MdHome} from "react-icons/md";

const Header = () => {
  return (
    <header className='header'>
      <NavLink activeClassName='link-check' exact className='link' to='/'><MdHome/>商城</NavLink>
      <NavLink activeClassName='link-check' exact className='link' to='/orders'><MdAddShoppingCart/>订单</NavLink>
      <NavLink activeClassName='link-check' exact className='link' to='/create'><MdAdd/>添加商品</NavLink>
    </header>
  )
}

export default Header