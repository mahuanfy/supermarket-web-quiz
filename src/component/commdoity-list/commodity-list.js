import React, {Component} from 'react';
import {CommodityBox} from './commodity-box/commodity-box'
import {connect} from "react-redux";
import {getCommodities} from "../../action/commodity-action";
import './commodity-list.less';
import {createOrder} from "../../action/order-action";

class CommodityList extends Component {
  componentDidMount() {
    this.props.getCommodities();
  }

  handleCreateOrder(id) {
    this.props.createOrder(id);
  }

  render() {
    const {commodities} = this.props;
    return (
      <section className="commodity-list">
        <div className='commodity'>
          {
            commodities.map((commodity, index) => {
              return <CommodityBox handleCreateOrder={this.handleCreateOrder.bind(this)} key={index}
                                   commodity={commodity}/>
            })
          }
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => ({
  commodities: state.commodities
});
const mapDispatchToProps = (dispatch) => ({
  getCommodities: () => dispatch(getCommodities()),
  createOrder: (id) => dispatch(createOrder(id))
});
export default connect(mapStateToProps, mapDispatchToProps)(CommodityList);