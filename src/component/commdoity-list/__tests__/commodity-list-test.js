import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render } from '@testing-library/react';
import {CommodityBox} from "../commodity-box/commodity-box";
import {BrowserRouter} from "react-router-dom";

test('CommodityBox test', () => {
  // why test router at component test?
  const { getByTestId } = render(<BrowserRouter><CommodityBox commodity={{name:'xiao', price: 5.5}} /></BrowserRouter>);
  expect(getByTestId('name')).toHaveValue('xiao');
});
