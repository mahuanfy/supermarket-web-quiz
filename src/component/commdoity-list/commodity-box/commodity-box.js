import React from 'react'
import './commodity-box.less';
import {MdAddCircleOutline} from "react-icons/md";

export const CommodityBox = (props) => {
  const {id, name, price, unit, image} = props.commodity;
  return <section className='commodity-box'>
    <img src={image}/>
    <article className='commodity-describe'>
      <h1 data-testid="name">{name}</h1>
      <span>单价：{price}元/{unit}</span>
      <span className='commodity-add' onClick={()=>props.handleCreateOrder(id)}><MdAddCircleOutline/></span>
    </article>
  </section>
};